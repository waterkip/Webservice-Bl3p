#!/usr/bin/env perl

use strict;
use warnings;

# ABSTRACT: A simple bl3p API script
# PODNAME: loan.pl

use Getopt::Long;
use Pod::Usage;
use Config::Any;
use File::Spec::Functions qw(catfile);
use URI;
use Webservice::Bl3p::Private;

my $basedir = catfile($ENV{HOME}, qw (.config bl3p));

my %opts = (
    help      => 0,
    config    => catfile($basedir, 'webservice-bl3p.conf'),
);

{
    local $SIG{__WARN__};
    my $ok = eval {
        GetOptions(
            \%opts, qw(help privkey=s pubkey=s call=s param=s@ base-uri=s market=s)
        );
    };
    if (!$ok) {
        die($@);
    }
}

pod2usage(0) if ($opts{help});

if (!-f $opts{config} && -f catfile($basedir, $opts{config})) {
    $opts{config} = catfile($basedir, $opts{config});
}

if (-f $opts{config}) {
    my $config = Config::Any->load_files(
        {
            files        => [$opts{config}],
            use_ext      => 1,
            flatten_hash => 1,

        }
    )->[0]{ $opts{config} };

    my @keys = qw(pubkey privkey);
    foreach (keys %opts) {
        delete $config->{$_};
    }

    foreach (keys %$config) {

        # If an option is set multiple times in the config file, take
        # the last value and work with that
        $opts{$_} ||=
            ref $config->{$_} eq 'ARRAY'
            ? $config->{$_}[-1]
            : $config->{$_};
    }
}

my @required = qw(privkey pubkey call);
foreach (@required) {
    if (!defined $opts{$_}) {
        pod2usage(1);
    }
}

my %params;
if ($opts{param}) {
    foreach (@{$opts{param}}) {
        my ($key, $val) = split(/=/, $_);
        $params{$key} = $val;
    }

}

if ($opts{'base-uri'}) {
    $opts{'base-uri'} = URI->new($opts{'base-uri'});
}

my $bl3p = Webservice::Bl3p::Private->new(
    pubkey  => $opts{pubkey},
    privkey => $opts{privkey},
    $opts{'base-uri'} ? (base_uri => $opts{'base-uri'}) : (),
);

if (my $code = $bl3p->can($opts{call})) {
    my $meth = $opts{call};
    if ($opts{market}) {
        $bl3p->$meth($opts{market}, %params);
    }
    else {
        $bl3p->$meth($opts{options}, %params);
    }
}
else {
    die "Unsupported call $opts{call} for Bl3p!", $/;
}


__END__

=head1 OPTIONS

=over

=item privkey

The private key of Bl3p

=item pubkey

The identifiation key of Bl3p

=item call

The name of the call

=item options

An array of parameters used to send data with the call

=item base-uri

Change the base URI for bl3p, defaults to https://api.bl3p.eu

