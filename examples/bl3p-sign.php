<?php

/* This code is here to validate our own signing. The code is extracted
* from the example code in use at:
* https://github.com/BitonicNL/bl3p-api/blob/master/examples/php/example.php
 */

function sign_bl3p($priv, $path, $params) {
    $post_data = http_build_query($params, '', '&');
    $body = $path . chr(0). $post_data;
    return base64_encode(hash_hmac('sha512', $body, base64_decode($priv), true));
}

$privkey = 'bXkgcHJpdmF0ZSBrZXkK';
$path    = 'GENMKT/money/wallet/history';
$params  = array( 'currency' => 'LTCEUR');

print sign_bl3p($privkey, $path, $params) . "\n";

?>
