use utf8;

package Webservice::Bl3p;
our $VERSION = '0.001';
use Moose;
use namespace::autoclean;

#use Types::Standard qw( Int Str );
#use Params::ValidationCompiler qw( validation_for );

use HTTP::Request;
use JSON;
use URI;
use LWP::UserAgent;
use List::Util qw(first);

# ABSTRACT: Query the Bl3p.eu API

__PACKAGE__->meta->make_immutable;

__END__

=head1 DESCRIPTION

A Perl API for L<https://bl3p.eu> crypto broker

=head1 SYNOPSIS

    use Webservice::Bl3p;


    my $public_api = Webservice::Bl3p->new();
    $public_api->ticker('BTCEUR');
    $public_api->orderbook('BTCEUR');
    $public_api->trades('LTCEUR');

    my $private_api = Webservice::Bl3p->new(
        privkey => 'foobar',
        pubkey  => 'foobar',
    );
    $private_api->private_call();

=head1 ATTRIBUTES

=head1 METHODS

=head2 api_call

Make an api call to the webservice

=head2 assert_cryptomarket

Assert if the market is a crypto market. Bl3p knows three market types:
C<BTCEUR>, C<LTCEUR> which are both crypto markets and it has C<GENMKT> which is a generic market. This is used for.. TODO: Look it up. This function will die when you supply anything other than BTCEUR or LTCEUR.

=head2 ticker

Show the ticker for the supplied market

=head2 orderbook

Show the orderbook for the supplied market

=head2 trades

Show the last 1000 trades for the supplied market

