use utf8;

package Webservice::Bl3p::Base;
our $VERSION = '0.001';
use Moose;
use namespace::autoclean;

#use Types::Standard qw( Int Str );
#use Params::ValidationCompiler qw( validation_for );

use HTTP::Request;
use JSON;
use LWP::UserAgent;
use List::Util qw(first);
use Try::Tiny;
use URI;

# ABSTRACT: Query the Bl3p.eu API

has base_uri => (
    is      => 'ro',
    isa     => 'URI',
    lazy    => 1,
    default => sub {
        return URI->new('https://api.bl3p.eu');
    }
);

has version => (
    is      => 'ro',
    isa     => 'Int',
    default => 1,
);

has ua => (
    is      => 'ro',
    isa     => 'LWP::UserAgent',
    lazy    => 1,
    builder => '_build_useragent',
);


sub _build_useragent {
    my $self = shift;
    my $ua   = LWP::UserAgent->new(
        ssl_opts => {
            SSL_ca_path     => '/etc/ssl/certs',
            verify_hostname => 1,
        }
    );
    return $ua;
}

sub api_call {
    my ($self, $market, $path, %params) = @_;

    # The examples show the nonce, but it doesn't seem to be needed if I
    # look at the code from npm and python. The python code does create
    # a nonce, but never uses it. Only the PHP example does something
    # with it and adds it to the params
    #my $nonce = $self->_get_nonce();
    #
    my $uri     = $self->_build_uri($market, $path, %params);
    my $request = $self->_build_request($uri, $market, $path);

    return $self->_call_bl3p($request);
}

sub _build_uri {
    my ($self, $market, $path, %params) = @_;
    my $uri = URI->new_abs(join('/', $self->version, $market, $path), $self->base_uri);
    $uri->query_form(\%params) if keys %params;
    return $uri;

}

sub _call_bl3p {
    my ($self, $request) = @_;

    my $res = $self->ua->request($request);
    return $self->_assert_response($res);
}

sub _assert_response {
    my ($self, $res) = @_;

    my $decoded = $res->decoded_content;

    if (!$res->is_success) {

        my $msg = sprintf("%s - %s", $res->status_line, $decoded);
        try {
            my $data = JSON->new->decode($decoded);
            $msg = sprintf("%s - %s", $data->{data}{code}, $data->{data}{message});
        }
        catch {
        };
        die $msg, $/;
    }

    my $data = JSON->new->decode($decoded) if $res->is_success;

    return $data unless exists $data->{result};
    if ($data->{result} eq 'error') {
        die sprintf("%s - %s", $data->{data}{code}, $data->{data}{message}), $/;
    }

    return $data->{data};
}

sub _build_request {
    my ($self, $uri) = @_;
    return HTTP::Request->new('GET', $uri);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 DESCRIPTION

The base class for Bl3p API usage. You'll want to override, around the
function C<_build_request> to support the private API of Bl3p

=head1 SYNOPSIS

    package Webservice::Bl3p::Foo;
    use Moose;
    extends 'Webservice::Bl3p::Base';

=head1 ATTRIBUTES

=head1 METHODS

=head2 api_call

Make an api call to the webservice

