use utf8;

package Webservice::Bl3p::Private;

use Moose;
extends 'Webservice::Bl3p::Public';
use namespace::autoclean;
use Digest::SHA qw(hmac_sha512);
use MIME::Base64;

# ABSTRACT: Query the private Bl3p.eu API

has pubkey => (
    is       => 'rw',
    isa      => 'Str',
    required => 1,
);

has privkey => (
    is       => 'rw',
    isa      => 'Str',
    required => 1,
);

around _build_request => sub {
    my $orig = shift;
    my $self = shift;
    my $uri  = shift;

    my $request = $self->$orig($uri);

    $request->header('Rest-Key'  => $self->pubkey);
    $request->header('Rest-Sign' => $self->_build_sign($uri));
    $request->method('POST');

    return $request;
};

sub _build_sign {
    my ($self, $uri) = @_;

    my $key = decode_base64($self->privkey);
    my $version = $self->version;
    my $path    = $uri->path;
    $path       =~ s#/$version/##;
    my $body    = $path . chr(0) . ($uri->query // '');

    my $sign = encode_base64(hmac_sha512($body, $key), '');
    return $sign;
}

sub order_add {
    my ($self, $market, %params) = @_;
    return $self->_crypto_api($market, 'money/order/add', %params);
}

sub order_cancel {
    my ($self, $market, %params) = @_;
    return $self->_crypto_api($market, 'money/order/cancel', %params);
}

sub order_info {
    my ($self, $market, %params) = @_;
    return $self->_crypto_api($market, 'money/order/result', %params);
}

sub full_depth {
    my ($self, $market, %params) = @_;
    return $self->_crypto_api($market, 'money/depth/full', %params);
}

sub deposit_address_get_new {
    my ($self, $market, %params) = @_;
    return $self->_crypto_api($market, 'money/new_deposit_address', %params);
}

sub deposit_address_get_last {
    my ($self, $market, %params) = @_;
    return $self->_crypto_api($market, 'money/deposit_address', %params);
}

sub fetch_trades {
    my ($self, $market, %params) = @_;
    return $self->_crypto_api($market, 'money/trades/fetch', %params);
}

sub wallet_history {
    my ($self, $market, %params) = @_;
    return $self->_account_api($market, 'money/wallet/history', %params);
}

sub orders {
    my ($self, $market) = @_;
    return $self->_crypto_api($market, 'money/orders');
}

sub _account_api {
    my ($self, $market, $call, %params) = @_;
    return $self->api_call('GENMKT', $call, %params);
}

sub _crypto_api {
    my ($self, $market, $call, %params) = @_;
    $self->assert_cryptomarket($market);
    return $self->api_call($market, $call, %params);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 DESCRIPTION

A Perl API for L<https://bl3p.eu> crypto broker

=head1 SYNOPSIS

    use Webservice::Bl3p::Private;


    my $private_api = Webservice::Bl3p->new(
        privkey => 'foobar',
        pubkey  => 'foobar',
    );
    $private_api->private_call();

=head1 ATTRIBUTES

=head2 pubkey

=head2 privkey

=head1 METHODS

