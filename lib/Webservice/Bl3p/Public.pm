use utf8;
package Webservice::Bl3p::Public;

use Moose;
extends 'Webservice::Bl3p::Base';

# ABSTRACT: Query the public Bl3p.eu API

use namespace::autoclean;

use List::Util qw(first);


sub assert_cryptomarket {
    my ($self, $market) = @_;
    if (first { $_ eq $market } qw(BTCEUR LTCEUR)) {
        return 1;
    }
    die "Invalid market '$market' selected";
}

sub ticker {
    my ($self, $market) = @_;
    return $self->_public_api($market, 'ticker');
}

sub orderbook {
    my ($self, $market) = @_;
    return $self->_public_api($market, 'orderbook');
}

sub trades {
    my ($self, $market) = @_;
    return $self->_public_api($market, 'trades');
}

sub _public_api {
    my ($self, $market, $call) = @_;
    $self->assert_cryptomarket($market);
    return $self->api_call($market, $call);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 DESCRIPTION

A Perl API for L<https://bl3p.eu> crypto broker

=head1 SYNOPSIS

    use Webservice::Bl3p::Public;


    my $public_api = Webservice::Bl3p::Public->new();
    $public_api->ticker('BTCEUR');
    $public_api->orderbook('BTCEUR');
    $public_api->trades('LTCEUR');

=head1 ATTRIBUTES

=head1 METHODS

=head2 assert_cryptomarket

Assert if the market is a crypto market. Bl3p knows three market types:
C<BTCEUR>, C<LTCEUR> which are both crypto markets and it has C<GENMKT> which is a generic market. This is used for.. TODO: Look it up. This function will die when you supply anything other than BTCEUR or LTCEUR.

=head2 ticker

Show the ticker for the supplied market

=head2 orderbook

Show the orderbook for the supplied market

=head2 trades

Show the last 1000 trades for the supplied market

