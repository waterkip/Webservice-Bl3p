use utf8;
use strict;
use warnings;
use Test::More 0.96;
use Test::Exception;
use Test::Deep;

use Webservice::Bl3p::Base;
use Test::Mock::One;
use Sub::Override;

my $bl3p = Webservice::Bl3p::Base->new();
isa_ok($bl3p, 'Webservice::Bl3p::Base');
is($bl3p->base_uri->as_string, "https://api.bl3p.eu", "We have a correct base uri");

{
    note "Method testing";

    my @methods = qw(
        _assert_response
        _build_request
        _build_useragent
        _call_bl3p
        api_call
    );

    foreach (@methods) {
        can_ok($bl3p, $_);
    }
}

{

    note "_assert_response";

    my $response = HTTP::Response->new(
        200, undef, undef, '{"foo":"bar"}',
    );

    lives_ok(
        sub {
            my $data = $bl3p->_assert_response($response);
            cmp_deeply($data, { foo => 'bar' }, "Correct Bl3p call returns valid JSON");
        },
        "Empty JSON is valid"
    );

    my $error = q#{"result": "error", "data": { "code": "KEY_MISSING", "message": "Rest-Key missing" } }#;

    $response = HTTP::Response->new(
        200, undef, undef, $error,
    );

    throws_ok(
        sub {
            $bl3p->_assert_response($response);
        },
        qr/^KEY_MISSING - Rest-Key missing$/,
        "API call is incorrect"
    );

    throws_ok(
        sub {
            $bl3p->_assert_response(HTTP::Response->new('400', undef, undef, '400 thrown by server'));
        },
        qr/^400 Bad Request - 400 thrown by server$/,
        "HTTP response isn't success"
    );
}


{
    note "_build_uri";

    my $snowman = $bl3p->_build_uri('market', 'foo', foo => 'bar', 'snowman' => '☃');
    is(
        $snowman->as_string,
        "https://api.bl3p.eu/1/market/foo?foo=bar&snowman=%E2%98%83",
        "Builds the correct URI (UTF8)"
    );

    note "_build_request";

    my $uri = $bl3p->_build_uri('BTCEUR', 'ticker');

    my $req = $bl3p->_build_request($uri);

    note "_call_bl3p";
    my $override = Sub::Override->new(
        'LWP::UserAgent::request' => sub {
            return HTTP::Response->new(
                200, undef, undef, '{}',
            );
        }
    );

    my $data = $bl3p->_call_bl3p($req);
}

done_testing;
