use utf8;
use strict;
use warnings;
use Test::More 0.96;
use Test::Exception;
use Test::Deep;

use IO::All;
use Sub::Override;
use Webservice::Bl3p::Public;

my $bl3p = Webservice::Bl3p::Public->new();
isa_ok($bl3p, 'Webservice::Bl3p::Public');

{
    note "assert_cryptomarket";

    foreach (qw(LTCEUR BTCEUR)) {
        lives_ok(
            sub {
                $bl3p->assert_cryptomarket($_);
            },
            "$_ is a valid crypto market"
        );
    }

    throws_ok(
        sub { $bl3p->assert_cryptomarket('GENMKT') },
        qr/Invalid market 'GENMKT' selected/,
        "GENMKT isn't a valid crypto market"
    );

}

{
    note "_public_api";

    my $override = Sub::Override->new(
        "Webservice::Bl3p::Base::api_call" => sub {
            my ($self, $market, $path, %params) = @_;
            is($market, "FOO", "Correct market ($market) for _public_api");
            is($path,   "foo", "Correct path ($path) for _public_api");
            cmp_deeply(\%params, {}, "No params giving");
        }
    );
    $override->override('Webservice::Bl3p::Public::assert_cryptomarket' => sub {
        return 1;
    });

    $bl3p->_public_api('FOO', "foo");
}

{
    note "Public methods";
    foreach my $meth (qw(ticker orderbook trades)) {
        can_ok($bl3p, $meth);
    }
}


done_testing;
