use utf8;
use strict;
use warnings;
use Test::More 0.96;
use Test::Exception;
use Test::Deep;

use IO::All;
use Sub::Override;
use Webservice::Bl3p::Private;
use URI;

my $bl3p = Webservice::Bl3p::Private->new(
    privkey => 'bXkgcHJpdmF0ZSBrZXkK',
    pubkey  => 'my public key',
);

isa_ok($bl3p, 'Webservice::Bl3p::Private');

{
    note "_build_request around adds rest-key and rest-sign headers";

    my $override = Sub::Override->new(
        'Webservice::Bl3p::Private::_build_sign' => sub { return 'sign' }
    );

    my $req = $bl3p->_build_request($bl3p->base_uri);

    my $headers = $req->headers();
    is($req->method, "POST", "Private URI is POST only");
    is($headers->header('rest-key'), 'my public key', "Rest-Key has correct value");
    is($headers->header('rest-sign'), 'sign', "Rest-Sign has correct value");
}

{
    note "_build_sign";

    my $uri = $bl3p->_build_uri('market', 'foo', foo => 'bar', 'snowman' => '☃');

    my $sign = $bl3p->_build_sign($uri);

    my $want = q{x+HOIuDkvzGcg9jRjfyvAPttjaqIDucFojTT/ZHz74IokxzoBVLYgaKWCNj/5aDr9luTxrnyGr2hsQA0gNBp6A==};
    is($sign, $want, "Correctly signed the message");

    {
        my $uri = $bl3p->_build_uri('market', 'foo');
        my $want = q{MUaO0OwplF8WYNXtMoCiRUUuJOdyZUDDPZv0zn8V90g2KR6o9gj9hCWkUahpkyqGNUuxloUzgH4GnrdRAb4UPA==};

        my $sign = $bl3p->_build_sign($uri);
        is($sign, $want, "Correctly signed the message");
    }
}

{
    test_api_call_ok(
        'order_add',
        'BTCEUR',
        {
            type             => 'ask',
            amount_int       => 1,
            price_int        => 1,
            amount_funds_int => 1,
            fee_currency     => 'BTC',
        },
        {
            market => 'BTCEUR',
            path   => 'money/order/add',
            params => {
                type             => 'ask',
                amount_int       => 1,
                price_int        => 1,
                amount_funds_int => 1,
                fee_currency     => 'BTC',
            }
        },
        'order_add: ask - with all options enabled',
    );

    test_api_call_ok(
        'order_add',
        'LTCEUR',
        {
            type             => 'bid',
            amount_int       => 1,
            price_int        => 1,
            amount_funds_int => 1,
            fee_currency     => 'EUR',
        },
        {
            market => 'LTCEUR',
            path   => 'money/order/add',
            params => {
                type             => 'bid',
                amount_int       => 1,
                price_int        => 1,
                amount_funds_int => 1,
                fee_currency     => 'EUR',
            }
        },
        'order_add: bid - with all options enabled',
    );

    test_api_call_ok(
        'order_add',
        'LTCEUR',
        {
            type             => 'bid',
            amount_int       => 1,
            price_int        => 1,
            amount_funds_int => 1,
            fee_currency     => 'EUR',
        },
        {
            market => 'LTCEUR',
            path   => 'money/order/add',
            params => {
                type             => 'bid',
                amount_int       => 1,
                price_int        => 1,
                amount_funds_int => 1,
                fee_currency     => 'EUR',
            }
        },
        'order_add: bid - with all options enabled',
    );
}

sub test_api_call_ok {
    my ($call, $market, $params, $expect, $testname) = @_;
    return ;

    note $testname;

    my $override = Sub::Override->new(
        "Webservice::Bl3p::Base::api_call" => sub {
            my ($self, $market, $path, %params) = @_;
            is($market, $expect->{market}, "Correct market ($market) for $call");
            is($path,   $expect->{path}, "Correct path ($path) for $call");
            cmp_deeply(\%params, $expect->{params}, "Correct params for $call");
        }
    );

    return lives_ok(
        sub {
            $bl3p->$call($market, %$params);
        },
        "$call works as intented"
    );
}

done_testing;
